.. module:: magnumnp

###########
Field Terms
###########

blah blah 

******************
Linear Field Terms
******************
more blah blah


***************
Class Reference
***************

FieldTerm
=========
.. autoclass:: FieldTerm

LinearFieldTerm
===============
.. autoclass:: LinearFieldTerm
   :show-inheritance:

UniaxialAnisotropyField
=======================
.. autoclass:: UniaxialAnisotropyField
   :show-inheritance:

CubicAnisotropyField
====================
.. autoclass:: CubicAnisotropyField
   :show-inheritance:

DemagField
==========
.. autoclass:: DemagField
   :show-inheritance:

ExchangeField
=============
.. autoclass:: ExchangeField
   :show-inheritance:

ExternalField
=============
.. autoclass:: ExternalField
   :show-inheritance:

DMIField
========
.. autoclass:: DMIField
   :show-inheritance:

BulkDMIField
------------
.. autoclass:: BulkDMIField
   :show-inheritance:

InterfaceDMIField
-----------------
.. autoclass:: InterfaceDMIField
   :show-inheritance:

D2dDMIField
-----------
.. autoclass:: D2dDMIField
   :show-inheritance:

RKKYField
============
.. autoclass:: RKKYField

SpinOrbitTorque
===============
.. autoclass:: SpinOrbitTorque

SpinTorqueZhangLi
=================
.. autoclass:: SpinTorqueZhangLi

OerstedField
============
.. autoclass:: OerstedField
